/*
* config.Db contient les parametres de connection à la base de données
* il va créer aussi un pool de connexions utilisables
* sa méthode getConnection permet de se connecter à MySQL
*
*/
var db = require('../configDb');

/*
* Récupérer l'intégralité les écuries avec l'adresse de la photo du pays de l'écurie
* @return Un tableau qui contient le N°, le nom de l'écurie et le nom de la photo du drapeau du pays
*/
module.exports.getListeEcurie = function (callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						var sql ="SELECT ecunum, payadrdrap, ecunom FROM ecurie e INNER JOIN pays p ";
						sql= sql + "ON p.paynum=e.paynum ORDER BY ecunom";
						//console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};


module.exports.getDetailEcurie = function (ecuNum, callback) {
   // connection à la base
  db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            var sql ="SELECT fpnom, ecunum, paynom, ecunom, ecunomdir, ecuadrsiege, ecuadresseimage";
            sql=sql + " FROM pays p INNER JOIN ecurie e ON p.paynum=e.paynum";
            sql= sql + " INNER JOIN  fourn_pneu fp ON e.fpnum=fp.fpnum WHERE ecunum= "+ecuNum+" ";
            //console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};


module.exports.getPilote = function (ecuNum, callback) {
   // connection à la base
  db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
              var sql ="SELECT e.ecunum, pilnom, p.pilnum, pilprenom, phoadresse";
              sql=sql + " FROM ecurie e INNER JOIN pilote p ";
              sql= sql + "ON p.ecunum=e.ecunum INNER JOIN photo ph ON p.PILNUM = ph.PILNUM WHERE p.ecunum= "+ecuNum+" ";
              sql= sql+ " AND PHONUM='1'";
            //console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.getVoiture = function (ecuNum, callback) {
   // connection à la base
  db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            var sql ="SELECT e.ecunum, voinum, voinom, voiadresseimage, typelibelle";
            sql=sql + " FROM ecurie e INNER JOIN voiture v ";
            sql= sql + "ON e.ecunum=v.ecunum INNER JOIN type_voiture tp ON "
            sql= sql + " v.typnum = tp.typnum WHERE e.ecunum= "+ecuNum+" ";
            //console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.getSponsor = function (ecuNum, callback) {
   // connection à la base
  db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            var sql ="SELECT e.ecunum, sponom, s.sponum";
            sql=sql + " FROM ecurie e INNER JOIN finance f ";
            sql= sql + "ON e.ecunum=f.ecunum INNER JOIN sponsor s ";
            sql= sql + " ON f.sponum=s.sponum WHERE e.ecunum= "+ecuNum+" ";
            //console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};