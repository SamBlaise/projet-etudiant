var db = require('../configDb');

module.exports.getListeResultat = function (callback) {

  db.getConnection(function(err, connexion){
        if(!err){
            var sql ="SELECT payadrdrap, gpnum, gpnom, c.cirnum, gpdate, gpcommentaire";
            sql=sql+ " FROM grandprix gp INNER JOIN circuit c ON  gp.cirnum=c.cirnum";
            sql= sql+ " INNER JOIN pays p ON p.paynum=c.paynum ORDER BY c.cirnom";
            console.log(sql);
            connexion.query(sql, callback);
            connexion.release();
         }
      });
};

module.exports.getDetailCircuit = function (gpNum, callback) {

  db.getConnection(function(err, connexion){
        if(!err){
            var sql ="SELECT gpnum, gpnom, DATE_FORMAT(gpdate, '%d/%m/%Y') AS gp, gpcommentaire";
            sql=sql+ " FROM grandprix WHERE gpnum= "+gpNum+" ";
            console.log(sql);
            connexion.query(sql, callback);
            connexion.release();
         }
      });
};

module.exports.getDetailResultat = function (gpNum, callback) {

  db.getConnection(function(err, connexion){
        if(!err){
            var sql ="SELECT pilnom, tempscourse, pilprenom";
            sql=sql+ " FROM course c INNER JOIN pilote p ON c.pilnum=p.pilnum";
            sql=sql+ " WHERE gpnum= "+gpNum+ " ORDER BY tempscourse LIMIT 10";
            console.log(sql);  
            connexion.query(sql, callback);
            connexion.release();
         }
      });
};


module.exports.getListePoint = function (callback) {

  db.getConnection(function(err, connexion){
        if(!err){
            var sql ="SELECT ptnbpointsplace FROM POINTS";
            //console.log(sql);  
            connexion.query(sql, callback);
            connexion.release();
         }
      });
};