/*
* config.Db contient les parametres de connection à la base de données
* il va créer aussi un pool de connexions utilisables
* sa méthode getConnection permet de se connecter à MySQL
*
*/
var db = require('../configDb');

/*
*
*/
module.exports.getListeInitial = function (callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						var sql ="SELECT DISTINCT LEFT (PILNOM, 1) AS Initial FROM pilote";
            sql= sql+ " ORDER BY 1 ASC";
						//console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.getListePilote = function (lettre, callback) {
   // connection à la base
  db.getConnection(function(err, connexion){
        if(!err){
            // s'il n'y a pas d'erreur de connexion
            // execution de la requête SQL
            var sql ="SELECT p.PILNUM, PILNOM, PILPRENOM, PHOADRESSE FROM pilote";
            sql= sql+ " p INNER JOIN photo ph ON p.PILNUM=ph.PILNUM";
            sql= sql+ " WHERE PILNOM LIKE '"+lettre+"%' ";
             sql= sql+ " AND PHONUM='1'";
            //console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.getDetailPilote = function(pilNum, callback){
  db.getConnection(function(err, connexion){
    if(!err){
       // s'il n'y a pas d'erreur de connexion
       // execution de la requête SQL
      var sql = "SELECT pi.PILNUM, PILNOM, PILPRENOM, PILTEXTE,"
      sql = sql+" DATE_FORMAT(PILDATENAIS, '%d/%m/%Y') AS PILDATENAIS, PILPOIDS, PILTAILLE,";
      sql = sql+" PAYNAT, ph.PHOADRESSE FROM pilote pi, pays pa, photo ph ";
      sql = sql+" WHERE pi.PILNUM = "+pilNum+" ";
      sql = sql+" AND pi.PAYNUM = pa.PAYNUM AND ph.PILNUM=pi.PILNUM AND ph.PHONUM = 1"

       //console.log (sql);
       connexion.query(sql, callback);

       // la connexion retourne dans le pool
       connexion.release();
      }
  });
};

module.exports.getEcurie = function(pilNum, callback){
  db.getConnection(function(err, connexion){
    if(!err){
       // s'il n'y a pas d'erreur de connexion
       // execution de la requête SQL
      var sql = "SELECT ECUNOM"
      sql = sql+" FROM ecurie e INNER JOIN pilote p ON e.ECUNUM=p.ECUNUM ";
      sql = sql+" WHERE p.PILNUM = "+pilNum+" ";
       //console.log (sql);
       connexion.query(sql, callback);

       // la connexion retourne dans le pool
       connexion.release();
      }
  });
};

module.exports.getSponsor = function(pilNum, callback){
  db.getConnection(function(err, connexion){
    if(!err){
       // s'il n'y a pas d'erreur de connexion
       // execution de la requête SQL
      var sql = "SELECT SPONOM, SPOSECTACTIVITE FROM sponsor s INNER JOIN sponsorise sp ON s.SPONUM=sp.SPONUM"
      sql = sql+" INNER JOIN pilote p ON sp.PILNUM=p.PILNUM";
      sql = sql+" WHERE p.PILNUM = "+pilNum+" ";
       //console.log (sql);
       connexion.query(sql, callback);

       // la connexion retourne dans le pool
       connexion.release();
      }
  });
};

module.exports.getPhoto = function(pilNum, callback){
  db.getConnection(function(err, connexion){
    if(!err){
      var sql = "SELECT PHOADRESSE FROM photo WHERE PILNUM = "+pilNum+" AND PHONUM != 1";
      console.log(sql);
      connexion.query(sql, callback);
      connexion.release();
    }
  });
};

