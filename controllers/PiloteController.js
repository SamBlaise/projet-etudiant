var model = require('../models/pilote.js');
var async=require('async');

//////////////////////////////L I S T E   D E S  I N I T I A L E S  D E S  P I L O T E S

module.exports.ListeInitial = 	function(request, response){
   response.title = 'Répertoire des pilotes';
   	 model.getListeInitial( function (err, result) {
        if (err) {
            console.log(err);
            return;
        }
        response.repertoirePilotes = result;
        response.render('repertoirePilotes', response);
      });
  } ;

//////////////////////////////R E P E R T O I R E    D E S    P I L O T E S

module.exports.Repertoire = function(request, response){
	response.title = 'Répertoire des pilotes';
	var lettre = request.params.initial;
  async.parallel ([
    function (callback){
      model.getListeInitial( function (err, result) {callback(null, result)});

    },

        function(callback){
           model.getListePilote(lettre, function (err, result) {callback(null, result)});
        },
      ],
        function(err, result){
           if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
          }
       	response.repertoirePilotes = result[0];
        response.listePilotes = result[1];
        response.render('repertoirePilotes', response);
        });
  };


////////////////////////D E T A I L  D U N  P I L O T E
  module.exports.DetailPilote = function(request, response){
    response.title = 'Répertoire des pilotes';
    var pilNum = request.params.numPil;
    //console.log(lettre);
     async.parallel ([
        function (callback){
          model.getListeInitial( function (err, result) {callback(null, result)});
        },

        function (callback){
           model.getDetailPilote(pilNum, function (err, result) {callback(null, result)});
        },

        function (callback){
          model.getEcurie(pilNum, function (err, result){callback(null, result)});
        },

        function (callback){
          model.getSponsor(pilNum, function (err, result){callback(null, result)});
        },

        function(callback){
          model.getPhoto(pilNum, function (err, result){callback(null, result)});
        },
      ],
        function(err, result){
           if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
          }
        response.repertoirePilotes = result[0];
        response.detailPilote = result[1][0];
        response.ListEcurie = result[2][0];
        response.ListSponsor = result[3];
        response.ListPhoto = result[4];
        response.render('detailPilote', response);
      });
  };