var model = require('../models/circuit.js');
var async=require('async');

//////////////////////////////L I S T E R     C I R C U I T S

module.exports.ListerCircuit = function(request, response){
  response.title = 'Liste des circuits';
    
    model.getListeCircuit( function (err, result) {
      if (err) {
        console.log(err);
        return;
      }
      response.listeCircuit = result;
      response.render('listerCircuit', response);
    });
};
  

//////////////////////////////D E T A I L  C I R C U I T

module.exports.DetailCircuit = function(request, response){
	response.title = 'Liste des circuits';
	
    var circuitNum = request.params.numCircuit;
    async.parallel ([
      function (callback){
        model.getListeCircuit( function (err, result) {callback(null, result)});
      },

      function(callback){
        model.getDetailCircuit(circuitNum, function (err, result) {callback(null, result)});
      },
    ],
    
    function(err, result){
      if (err) {
        console.log(err);
        return;
      }
     	response.listeCircuit = result[0];
      response.detailCircuit = result[1][0];
      response.render('detailCircuit', response);
    });
};
