var model = require('../models/resultat.js');
var async=require('async');

//////////////////////////////L I S T E R    R E S U L T A T S

module.exports.ListerResultat = function(request, response){
  response.title = 'Liste des résulats des grands prix';

    model.getListeResultat( function (err, result) {
      if (err) {
        console.log(err);
        return;
      }
      response.listerResultat=result;
    response.render('listerResultat', response);
    });
};

//////////////////////////////D E T A I L     R E S U L T A T S

module.exports.DetailResultat = function(request, response){
  response.title = 'Liste des ecurie';

    var gpNum = request.params.numGP;
    async.parallel ([
      function (callback){
        model.getListeResultat( function (err, result) {
          callback(null, result)
        });
      },
      
      function (callback){
        model.getDetailCircuit(gpNum, function (err, result) {
          callback(null, result)
       });
      },
      
      function (callback){
        model.getDetailResultat(gpNum, function (err, result) {
          callback(null, result)
        });
      },
      function (callback){
        model.getListePoint(function (err, result) {
          callback(null, result)
        });
      },
    ],
    
    function(err, result){
      if (err) {
        console.log(err);
        return;
      }
      response.listerResultat = result[0];
      response.detailCircuit = result[1][0];
       var tab= new Array; 
       console.log(result[2]);
      for(var i = 0; i < result[2].length; i++){
          //console.log(result[2][i]);
          //console.log(result[3][i]);

          result[2][i].point=result[3][i];
      }
      console.log(result[2]);
      response.detailResultat= result[2];
      response.render('detailGrandPrix', response);
    });
};