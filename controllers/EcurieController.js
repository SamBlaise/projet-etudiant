var model = require('../models/ecurie.js');
var async=require('async');

//////////////////////////////L I S T E   D E S    E C U R I E S

module.exports.ListerEcurie = function(request, response){
  response.title = 'Liste des écuries';

   model.getListeEcurie( function (err, result) {
      if (err) {
        console.log(err);
        return;
      }
      response.listeEcurie = result;
      response.render('listerEcurie', response);
    });
};

//////////////////////////////D E T A I L S    D E S    E C U R I E S

module.exports.DetailEcurie = function(request, response){
  response.title = 'Liste des ecurie';
  
    var ecuNum = request.params.numEcurie;
    async.parallel ([
      function (callback){
        model.getListeEcurie( function (err, result) {callback(null, result)});
      },

      function(callback){
        model.getDetailEcurie(ecuNum, function (err, result) {callback(null, result)});
      },
      
      function(callback){
        model.getVoiture(ecuNum, function (err, result) {callback(null, result)});
      },
      
      function(callback){
        model.getPilote(ecuNum, function (err, result) {callback(null, result)});
      },
      
      function(callback){
        model.getSponsor(ecuNum, function (err, result) {callback(null, result)});
      },
    ],

    function(err, result){
      if (err) {
        console.log(err);
        return;
      }
      response.listeEcurie = result[0];
      response.detailEcurie = result[1][0];
      response.listeVoiture = result[2];
      response.listePilote = result[3];
      response.listeSponsor = result[4];
      response.render('detailEcurie', response);
    });
};
